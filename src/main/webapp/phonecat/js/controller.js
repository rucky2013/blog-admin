/**
 * Created by Xulu on 2014/6/10.
 */
function PhoneListCtrl($scope, Services) {
    $scope.phones = Services.phone.query();

    $scope.orderProp = 'age';
    var p = {'tag.name':"ceshi", 'tag.count':1};
    Services.tag.add(p);
}

function PhoneDetailCtrl($scope, $routeParams, Services) {

    $scope.phone = Services.phone.get({phoneId:$routeParams.phoneId},function(phone){
        $scope.mainImageUrl = phone.images[0];
    });

    $scope.setImage = function(imageUrl) {
        $scope.mainImageUrl = imageUrl;
    }

}