var FormSamples = function () {
    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
            	format:'yyyy-mm-dd',
            	locale: {
	            	daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
	                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
            	},
                rtl : App.isRTL()
            });
        }
    }
    

    return {
        //main function to initiate the module
        init: function () {
        	$('#sample-form').validate(
                    {
                        errorElement : 'span', //default input error message container
                        errorClass : 'help-inline', // default input error message class
                        focusInvalid : false, // do not focus the last invalid input
                        ignore : "",

//                        messages : { // custom messages for radio buttons and checkboxes
//                            'product.name' : {
//                                required : "产品名称不能为空",
//                                minlength : jQuery
//                                        .format("产品名称不得少于两个字符")
//                            },
//                            'product.category' : {
//                                required : "请选择产品类别"
//                            }
//                        },

                        highlight : function(element) { // hightlight error inputs
                            $(element).closest('.help-inline')
                                    .removeClass('ok'); // display OK icon
                            $(element).closest('.control-group')
                                    .removeClass('success').addClass(
                                            'error'); // set error class to the control group
                        },

                        unhighlight : function(element) { // revert the change dony by hightlight
                            $(element).closest('.control-group')
                                    .removeClass('error'); // set error class to the control group
                        },

                        success : function(label) {
                            label.addClass('valid').addClass(
                                    'help-inline ok') // mark the current input as valid and display OK icon
                            .closest('.control-group').removeClass(
                                    'error').addClass('success'); // set success class to the control group
                        },

                        submitHandler : function(form) {
                            success1.show();
                            error1.hide();
                        }
                    });
        	
        	handleDatePickers();
            // use select2 dropdown instead of chosen as select2 works fine with bootstrap on responsive layouts.
            $('.select2_category').select2({
	            placeholder: "Select an option",
	            allowClear: true
	        });

            $('.select2_sample1').select2({
                placeholder: "Select a State",
                allowClear: true
            });

            $(".select2_sample2").select2({
                placeholder: "Type to select an option",
                allowClear: true,
                minimumInputLength: 1,
                query: function (query) {
                    var data = {
                        results: []
                    }, i, j, s;
                    for (i = 1; i < 5; i++) {
                        s = "";
                        for (j = 0; j < i; j++) {
                            s = s + query.term;
                        }
                        data.results.push({
                            id: query.term + i,
                            text: s
                        });
                    }
                    query.callback(data);
                }
            });

            $(".select2_sample3").select2({
                tags: ["red", "green", "blue", "yellow", "pink"]
            });
            
        }

    };

}();