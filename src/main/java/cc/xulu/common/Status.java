package cc.xulu.common;

/**
 * Created by xulu on 14-6-18.
 */
public class Status {
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String message;
    public String status;

    public Status() {
        this.status = "1";
        this.message = "操作成功!";
    }

    public Status(String status, String message) {
        this.status = status;
        this.message = message;
    }


}
