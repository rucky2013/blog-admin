package cc.xulu.controller;

import cc.xulu.common.Status;
import com.jfinal.core.Controller;

import cc.xulu.model.*;

public class PostTagController extends Controller {
	public void index() {
		setAttr("post_tagPage", PostTag.dao.paginate(getParaToInt(0, 1), 10, "select *", "from post_tag order by id asc"));
		render("list.html");
	}
	
	public void add() {
	    render("edit.html");
	}

    public void edit() {
        setAttr("post_tag", PostTag.dao.findById(getParaToInt()));
        render("edit.html");
    }
	
	public void save() {
		PostTag post_tag = getModel(PostTag.class);

		post_tag.save();

        renderJson(post_tag);
	}

    public void show() {
        PostTag post_tag = PostTag.dao.findById(getParaToInt());

        renderJson(post_tag);

    }
	
	public void update() {
        PostTag post_tag = getModel(PostTag.class);
        post_tag.update();
        renderJson(new Status());
	}
	
	public void delete() {
        PostTag.dao.deleteById(getParaToInt());
        renderJson(new Status());
	}
}
